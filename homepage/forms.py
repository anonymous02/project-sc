from django import forms

pilihan = (
    (True, "Ya"),
    (False, "Tidak"),
    ("Tidak tahu", "Tidak tahu"),
)

pilihan_negara = (
    ("Malaysia", "Malaysia"),
    ("China", "China"),
    ("Taiwan", "Taiwan"),
    ("Singapore", "Singapore"),
    ("Vietnam", "Vietnam"),
    ("Myanmar", "Myanmar"),
    ("Amerika", "Amerika"),
    ("Tidak ada", "Tidak ada"),
)

class KondisiForm(forms.Form):
    kontak = forms.ChoiceField(choices = pilihan)
    mengunjungi = forms.ChoiceField(choices= pilihan_negara)
    demam = forms.ChoiceField(choices = pilihan)
    pilek = forms.ChoiceField(choices = pilihan)
    kelelahan = forms.ChoiceField(choices = pilihan)
    batuk = forms.ChoiceField(choices = pilihan)
    sesak = forms.ChoiceField(choices = pilihan)