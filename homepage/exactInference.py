from copy import deepcopy

class exactInference:    
    def __init__(self, kondisi):
        self.total = 0
        self.k = deepcopy(kondisi)
        self.kemungkinan = []

    def kontak(self, kondisi):
        if(kondisi['kontak'] == 'True'):
            return 0.9
        else:
            return 0.1

    def mengunjungi(self, kondisi):
        if(kondisi['mengunjungi'] == 'True'):
            return 0.7
        else:
            return 0.3

    def covid(self, kondisi):
        if(kondisi['covid'] == 'True'):    
            if(kondisi['kontak'] == 'True'):
                if(kondisi['mengunjungi'] == 'True'):
                    return 0.95
                else:
                    return 0.9
            else:
                if(kondisi['kontak'] == 'True'):
                    return 0.7
                else:
                    return 0.2
        else:
            if(kondisi['kontak'] == 'True'):
                if(kondisi['mengunjungi'] == 'True'):
                    return 0.05
                else:
                    return 0.1
            else:
                if(kondisi['kontak'] == 'True'):
                    return 0.3
                else:
                    return 0.8

    def demam(self, kondisi):
        if(kondisi['demam'] == 'True'):
            if(kondisi['covid'] == 'True'):
                return 0.8
            else:
                return 0.2
        else:
            if(kondisi['covid'] == 'True'):
                return 0.2
            else:
                return 0.8

    def pilek(self, kondisi):
        if(kondisi['pilek']):
            if(kondisi['covid'] == 'True'):
                return 0.6
            else:
                return 0.4
        else:
            if(kondisi['covid'] == 'True'):
                return 0.4
            else:
                return 0.6

    def kelelahan(self, kondisi):
        if(kondisi['kelelahan'] == 'True'):
            if(kondisi['covid'] == 'True'):
                return 0.6
            else:
                return 0.4
        else:
            if(kondisi['covid'] == 'True'):
                return 0.4
            else:
                return 0.6

    def sesak(self, kondisi):
        if(kondisi['sesak'] == 'True'):
            if(kondisi['covid'] == 'True'):
                if(kondisi['pilek'] == 'True'):
                    return 0.95
                else:
                    return 0.6
            else:
                if(kondisi['pilek'] == 'True'):
                    return 0.7
                else:
                    return 0.2
        else:
            if(kondisi['covid'] == 'True'):
                if(kondisi['pilek'] == 'True'):
                    return 0.05
                else:
                    return 0.4
            else:
                if(kondisi['pilek'] == 'True'):
                    return 0.3
                else:
                    return 0.8

    def batuk(self, kondisi):
        if(kondisi['batuk'] == 'True'):
            if(kondisi['covid'] == 'True'):
                if(kondisi['sesak'] == 'True'):
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.95
                    else:
                        return 0.8
                else:
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.7
                    else:
                        return 0.4
            else:
                if(kondisi['sesak'] == 'True'):
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.8
                    else:
                        return 0.7
                else:
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.6
                    else:
                        return 0.3
        else:
            if(kondisi['covid'] == 'True'):
                if(kondisi['sesak'] == 'True'):
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.05
                    else:
                        return 0.2
                else:
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.3
                    else:
                        return 0.6
            else:
                if(kondisi['sesak'] == 'True'):
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.2
                    else:
                        return 0.3
                else:
                    if(kondisi['kelelahan'] == 'True'):
                        return 0.4
                    else:
                        return 0.7

    def hitung(self, unknown, n):
        print(self.k)
        print("=============================================")
        self.total = 0
        print(self.total)
        self.count(unknown, n)

    def count(self, unknown, n):
        if(len(unknown) == n):
            print(self.total)
            self.total = self.total + self.kontak(self.k)*self.mengunjungi(self.k)*self.kelelahan(self.k)*self.demam(self.k)*self.pilek(self.k)*self.sesak(self.k)*self.covid(self.k)*self.batuk(self.k)
            print(self.total)
            print(self.kontak(self.k))
            print(self.mengunjungi(self.k))
            print(self.demam(self.k))
            print(self.pilek(self.k))
            print(self.kelelahan(self.k))
            print(self.sesak(self.k))
            print(self.covid(self.k))
            print(self.batuk(self.k))
            print("================================")
            return

        for i in ['True', 'False'] :
            self.k[unknown[n]] = i
            self.count(unknown, n+1)

    def result(self):
        return self.total