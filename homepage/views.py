from django.shortcuts import render
from django.http import HttpResponse
from .exactInference import exactInference
from .forms import KondisiForm

# Create your views here.
def index(request):
    context = {}
    context['form'] = KondisiForm()
    if request.GET: 
        k = {}
        k['kontak'] = request.GET['kontak']
        k['demam'] = request.GET['demam']
        k['pilek'] = request.GET['pilek']
        k['kelelahan'] = request.GET['kelelahan']
        k['batuk'] = request.GET['batuk']
        k['sesak'] = request.GET['sesak']
        negara = request.GET['mengunjungi']
        if(negara == "China" or negara == "Amerika"):
            k['mengunjungi'] = 'True'
        elif(negara == "Tidak ada"):
            k['mengunjungi'] = 'False'
        else:
            k['mengunjungi'] = 'Tidak tahu'

        k['covid'] = 'True'

        probability = probabilitas(k)
        return render(request, "result.html", {'probability' : probability})

    return render(request, "homepage.html", context)

def probabilitas(k):
    exact_inference = exactInference(k)

    known = []
    unknown = []
    for status in k:
        if(k[status] == 'Tidak tahu'):
            unknown.append(status)
        else:
            known.append(status)

    exact_inference.hitung(unknown, 0)
    kemungkinan = exact_inference.result()
    
    if(len(known) > 0):
        unknown.append('covid')
        exact_inference.hitung(unknown, 0)
        kemungkinan = kemungkinan / exact_inference.result()

    print(unknown)

    return kemungkinan